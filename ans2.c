#include <stdio.h>

// this will make the function easier to use
#define fibonacciSeq(x) fibSeq(x, 0) 

/* This function will return the fibonacci number according to the position
 * n = position
 */
int fib(int n)
{
    return n <= 1 ? n : fib(n - 1) + fib(n - 2);
}

/* This function will print fibonacci numbers upto n positions
 * n = how many fibonacci numbers needed to print
 * k = keep track of increasing order
 */
void fibSeq(int n, int k)
{
    if (n > -1)
    {
        printf("%d\n", fib(k));
        fibSeq(--n, ++k);
    }
}

int main()
{
    fibonacciSeq(10);

    return 0;
}
