#include <stdio.h>

// this will make the function easier to use
#define pattern(x) tree(x, 1)

/* This function will print the number triangle upto n
 * n = maximum number function make the pattern upto
 * k = keep track of how many numbers to print
 * e.x:
 * 1
 * 2 1
 * 3 2 1
 * 4 3 2 1
 */
void tree(int n, int k)
{
    if (n > 0)
    {
        for (int i = k; i > 0; i--)  // print the numbers according to each term
            printf("%d ", i);

        printf("\n");
        tree(--n, ++k);              // again call the function itself (recursively until n = 1)
    }
}

int main()
{
    pattern(4);                      // start the function with initial value 4

    return 0;
}
